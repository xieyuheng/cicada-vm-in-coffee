sad = require "sad-coffee"

{ in_node, in_browser,
  function_p, array_p, object_p, atom_p, string_p
  cat, orz, asr
  STACK, HASH_TABLE
  argack, retack
  into, out, ya, eva
} = sad
string_to_sum = (string) ->
  sum = 0
  max_step = 10
  i = 0
  while i < string.length
    sum = sum +
      string.codePointAt(i) *
      (2 << Math.min(i, max_step))
    i = 1 + i
  sum
tag_hash_table = new HASH_TABLE \
  # prime table size
  997
  # key_equal
, (key1, key2) ->
    key1 is key2
  # hash
, (key, counter) ->
    (counter + string_to_sum(key)) % 997

index = tag_hash_table.key_to_index("testkey0")
key = tag_hash_table.index_to_entry(index).key
asr(key is "testkey0")

# tag_hash_table.report()
array_equal = (a, b) ->
  if a is b
    return true
  if a is null or b is null
    return false
  if a.length isnt b.length
    return false
  i = 0
  while i < a.length
    if a[i] isnt b[i]
      return false
    i = 1 + i
  true

array_remove_dup = (orig_array) ->
  array = orig_array.map((element) -> return element)
  array.sort()
  result = []
  i = 0
  while i < array.length
    if result[result.length - 1] is array[i]
    else
      result.push(array[i])
    i = 1 + i
  result

array_remove = (array, value) ->
  result = []
  for element in array
    if element is value
      # nothing
    else
      result.push(element)
  result
name_hash_table = new HASH_TABLE \
  # prime table size
  997
  # key_equal
, (key1, key2) ->
    array_equal \
      array_remove_dup(key1),
      array_remove_dup(key2)
  # hash
, (key, counter) ->
    sum = array_remove_dup(key)
        .map(string_to_sum)
        .reduce((sum, v) -> sum + v)
    (counter + sum) % 997
string_to_key = (string) ->
  array_remove(string.split(":"), "")

key_to_string = (key) ->
  result = ""
  for element in key
    do (element) ->
    result = result.concat(":", element)
  result.substring(1)

name = (string) ->
  name_hash_table.key_to_index(string_to_key(string))

name_to_string = (name) ->
  key_to_string(name_hash_table.index_to_entry(name).key)
set_name = (name, value) ->
  name_hash_table.index_to_entry(name).value = value

get_name = (name) ->
  name_hash_table.index_to_entry(name).value
do ->
  name("testkey0")
  name("testkey1")
  name("testkey2")
  name("testkey3")
  name("testkey4")
  name("testkey1:testkey2:testkey3")
  name(":::testkey1:testkey2:testkey3")
  name("testkey1:testkey2")
  name("testkey4:testkey4:testkey1")
  name("testkey4:testkey1")

  asr(get_name(name("k1")) is null)

  set_name(name("k1:k1:k1"), 1)
  asr(get_name(name("k1")) is 1)

  set_name(name("k1:k1:k1"), null)
  asr(get_name(name("k1")) is null)

  # name_hash_table.report()
template_to_predicate = (template) ->
  recur = (template, object) ->
    for key of template
      do (key) ->
      if template[key] is true
        if object[key] is undefined
          return false
      else if object_p(template[key])
        if not object_p(object[key])
          return false
        success = recur(template[key], object[key])
        if not success
          return false
      else
        # do nothing
    return true
  return (object) ->
    if not object_p(object)
      false
    else
      recur(template, object)
type_constructor_template =
  type: true
  data: true
  check: true
  compute: true

type_constructor_p =
  template_to_predicate type_constructor_template
do ->
  testing_template =
    type: true
    data: true
    check: true
    compute: true
    message:
      testing_message1: true
      testing_message2: true

  testing_p =
    template_to_predicate testing_template

  asr not testing_p 123

  asr not testing_p
    type: new Object
    data: new Object
    check: new Object
    compute: new Object

  asr not testing_p
    type: new Object
    data: new Object
    check: new Object
    compute: new Object
    other: new Object

  asr not testing_p
    type: new Object
    data: new Object
    check: new Object
    other: new Object

  asr testing_p
    type: new Object
    data: new Object
    check: new Object
    compute: new Object
    message:
      testing_message1: new Object
      testing_message2: new Object

  asr testing_p
    type: new Object
    data: new Object
    check: new Object
    compute: new Object
    other: new Object
    message:
      testing_message1: new Object
      testing_message2: new Object

  asr not testing_p
    type: new Object
    data: new Object
    check: new Object
    other: new Object
    message:
      testing_message1: new Object
      testing_message2: new Object

  asr not testing_p
    type: new Object
    data: new Object
    compute: new Object
    check: new Object
    other: new Object
    message:
      testing_message1: new Object
      testing_message3: new Object
argument_stack = new STACK()

argument_stack.print = () ->
  index = 0
  arg_list = []
  while (index < @cursor())
    arg_list.push(@get(index))
    index = 1 + index
  arg_list =
    [(name_to_string arg[0]), arg[1]] for arg in arg_list
  arg_list.unshift("  *", @cursor(), "*  --")
  arg_list.push("--")
  console.log.apply console, arg_list
push = (value) -> argument_stack.push(value)
push_array = (array) -> argument_stack.push_array(array)

pop = () -> argument_stack.pop()
n_pop = (n) -> argument_stack.n_pop(n)

tos = () -> argument_stack.tos()
n_tos = (n) -> argument_stack.n_tos(n)
return_stack = new STACK()
class R
  constructor: (@jojo, @mode = "compute") ->
    @cursor = 0
    @local_variable_map = new Map()

  get_current_jo: () ->
    @jojo[@cursor]

  at_tail_position: () ->
    @cursor + 1 is @jojo.length

  next: () ->
    @cursor = 1 + @cursor
debugging = false
evajojo = (jojo) ->
  if jojo[0] isnt (name "jojo")
    orz "- evajojo\n",
        "  the argument is not a jojo\n"
        "  tag:", (name_to_string jojo [0])
        "  value:", jojo [1]
  base_cursor = return_stack.cursor()
  first_r = new R (jojo[1])
  return_stack.push first_r
  try
    while return_stack.cursor() > base_cursor
      r = return_stack.tos()
      jo = r.get_current_jo()
      if not r.at_tail_position()
        r.next()
      else
        return_stack.pop()
      get_name(jo[0])[r.mode](r, jo[1])
      if debugging
        argument_stack.print()
    return first_r
  catch string
    if string is "bye"
      return first_r
    else
      orz "- unknown exception:\n",
          "  ", string
set_name (name "name"),
type: null
data: null
check: null
compute: (r, the_name) ->
  g = get_name(the_name)
  get_name(g.data[0])["for_name"][r.mode](g, g.data[1])
set_name (name "primitive"),
type: null
data: null
check: null
for_name:
  compute: (g0, value) ->
    value.call()
compute: (r, value) ->
  value.call()
set_name (name "jojo"),
type: null
data: null
check: null
for_name:
  compute: (g0, value) ->
    return_stack.push new R (value)
compute: (r, value) ->
  return_stack.push new R (value)
set_name (name "integer"),
type: null
data: null
check: null
for_name:
  compute: (g0, value) ->
    argument_stack.push [(name "integer"), value]
compute: (r, value) ->
  argument_stack.push [(name "integer"), value]
set_name (name "local"),
type: null
data: null
check: null
for_name:
  compute: (g0, value) ->
compute: (r, value) ->
argument_stack_snapshot = 0

set_argument_stack_snapshot = () ->
  argument_stack_snapshot = argument_stack.cursor()
get_snapshot_different_number = () ->
  argument_stack.cursor() - argument_stack_snapshot
jo_equal = (jo1, jo2) ->
jo_unify = (jo1, jo2) ->
n_unify = (n) ->
  jo1 = [name("->"), n_pop(n)]
  jo2 = [name("->"), n_tos(n)]
  jo_unify(jo1, jo2)
set_name (name "->"),
type: [ name "primitive"
        null ]
data: [ name "primitive"
        () ->
          n = get_snapshot_different_number()
          r = return_stack.tos()
          jo = r.get_current_jo()
          r.next()
          if jo[0] is name("->")
            orz "- ->\n",
                "  what follows the -> should be a ->\n",
                "  instead of :", jo
            return
          success = n_unify(n)
          if success
            r.cursor = 0
            r.jojo = jo
          else
            r.next()
      ]
uni = (array1, array2) ->

set_name (name "sequent"),
type: null
data: null
check: null
for_name:
  compute: (g0, sequent) ->
    set_argument_stack_snapshot()
    r0 = evajojo sequent.antecedent
    n = get_snapshot_different_number()
    success = uni n_tos(n), n_pop(n)
    if (success)
      r = new R sequent.succedent
      r.local_variable_map = r0.local_variable_map
      return_stack.push r
    else
      throw "uni_fail"
compute: (r, sequent) ->
set_name (name "bye"),
type: [ name "primitive"
        null ]
data: [ name "primitive"
        () ->
          console.log("bye bye ^-^/")
          throw "bye"
      ]
set_name (name "dup"),
type: [ name "primitive"
        null ]
data: [ name "primitive"
        () ->
          a = tos()
          push(a)
      ]
set_name (name "mul"),
type: [ name "primitive"
        null ]
data: [ name "primitive"
        () ->
          a1 = pop()
          a2 = pop()
          push [ name "integer"
                 a1[1] * a2[1] ]
      ]
set_name (name "simple-wirte"),
type: [ name "primitive"
        null ]
data: [ name "primitive"
        () ->
          a = pop()
          cat name_to_string(a[0]),
              a[1]
      ]
space_char_p = (char) ->
  if char is "\n"
    true
  else if char is "\t"
    true
  else if char is " "
    true
  else
    false
braket_char_p = (char) ->
  if char is "("
    true
  else if char is ")"
    true
  else if char is "["
    true
  else if char is "]"
    true
  else if char is "{"
    true
  else if char is "}"
    true
  else
    false
double_quote_char_p = (char) ->
  if char is '"'
    true
  else
    false
number_char_p = (char) ->
  (char.codePointAt(0) >= "0".codePointAt(0)) and
  (char.codePointAt(0) <= "9".codePointAt(0))
space_string_p = (string) ->
  for char in string
    do (char) ->
    if space_char_p(char)
      # do nothing
    else
      return false
  return true
number_string_p = (string) ->
  if string.length is 0
    return false
  else
    for char in string
      do (char) ->
      if not number_char_p(char)
        return false
    return true

do ->
  asr(number_string_p("123") is true)
  asr(number_string_p("-123") is false)
integer_string_p = (string) ->
  if string.length is 0
    return false
  else if string.length is 1
    return number_char_p(string)
  else if string.substring(0, 1) is "-"
    return number_string_p(string.substring(1))
  else
    return number_string_p(string)

do ->
  asr(integer_string_p("123") is true)
  asr(integer_string_p("-123") is true)
string_to_vm_integer = (string) ->
  [name("integer"), parseInt(string, 10)]
keyword_processer_stack = new STACK()
process_word = (word) ->
  cursor = keyword_processer_stack.cursor()
  while (cursor isnt 0)
    cursor = cursor - 1
    keyword_processer =
        keyword_processer_stack.get(cursor)
    if keyword_processer.predicate(word)
      return keyword_processer.syntax_function(word)
  return null
keyword_record = new Map()
syntax_round_bra = () ->
  word = current_reading_buffer.read_word()
  result = process_word(word)
  if result isnt null
    return result
  else
    orz("- syntax_round_bra\n",
        "  do not know how to process word:", word, "\n",
        "  cursor:", current_reading_buffer.cursor)
#  [READING_BUFFER] -> [set_name name_hash_table]
syntax_defjojo = () ->
  name_string = current_reading_buffer.read_word()
  result = process_word(current_reading_buffer.read_word())
  if result isnt null
    set_name (name (name_string)),
      type: [ name("type"), null ]
      data: result
  else
    orz "- syntax_defjojo\n",
        "  result of process_word:", result, "\n",
        "  cursor:", current_reading_buffer.cursor
#  [READING_BUFFER] -> [set_name name_hash_table]
syntax_defdata = () ->
  name_string = current_reading_buffer.read_word()
  result_type = [] # ><><>< bo be handled
  result = process_word(current_reading_buffer.read_word())
  if result isnt null
    set_name (name (name_string)),
      type: [ name("type"), result_type ]
      data: result
  else
    orz "- syntax_defdata\n",
        "  result of process_word:", result, "\n",
        "  cursor:", current_reading_buffer.cursor
#  [READING_BUFFER] -> data
syntax_jojo = () ->
  array = []
  while (true)
    word = current_reading_buffer.read_word()
    if word is "]"
      return [name("jojo"), array]
    else
      result = process_word(word)
      if result isnt null
        array.push(result)
      else
        array.push([name("name"), name(word)])
#  [READING_BUFFER] -> (r)
syntax_evajojo = () ->
  result = process_word(current_reading_buffer.read_word())
  if result isnt null
    evajojo(result)
  else
    orz "- syntax_evajojo\n",
        "  result of process_word:", result, "\n",
        "  cursor:", current_reading_buffer.cursor
# keyword_record.set "(", syntax_round_bra
keyword_record.set "defjojo", syntax_defjojo
keyword_record.set "defdata", syntax_defdata
keyword_record.set "[", syntax_jojo
keyword_record.set "evajojo", syntax_evajojo
keyword_processer_stack.push
  predicate: (word) ->
    keyword_record.has(word)
  syntax_function: (word) ->
    keyword_record.get(word).call()

keyword_processer_stack.push
  predicate: integer_string_p
  syntax_function: string_to_vm_integer
class READING_BUFFER
  constructor: (@name, @string) ->
    @size = @string.length
    @cursor = 0

  at_start: () ->
    @cursor is 0

  at_end: () ->
    @cursor is @size

  space: () ->
    space_string_p @string.substring(@cursor)

  read_char: () ->
    if @at_end()
      cat("- read_char fail\n",
          "  at the end of the READING_BUFFER", @name)
    else
      @cursor = 1 + @cursor
      @string.substring(@cursor - 1, @cursor)

  back_char: () ->
    if @at_start()
      cat("- back_char fail\n",
          "  at the start of the READING_BUFFER:", @name)
    else
      @cursor = @cursor - 1

  # recursive helper function
  to_word_start: () ->
    if @at_end()
      # do nothing
    else
      char = @read_char()
      if space_string_p(char)
        @to_word_start()
      else
        @back_char()

  # recursive helper function
  to_word_end: () ->
    if @at_end()
      # do nothing
    else
      char = @read_char()
      if space_string_p(char)
        @back_char()
      else if braket_char_p(char)
        @back_char()
      else if double_quote_char_p(char)
        @back_char()
      else
        @to_word_end()

  read_word: () ->
    @to_word_start()
    if @space()
      cat("- read_word fail\n",
          "  only space left in the READING_BUFFER:", @name)
    else
      char = @read_char()
      if braket_char_p(char)
        char
      else if double_quote_char_p(char)
        char
      else
        word_start = @cursor - 1
        @to_word_end()
        @string.substring(word_start, @cursor)

k1 = new READING_BUFFER "k1", "一二三 \n 三二一"
asr(k1.read_word() is "一二三")
asr(k1.read_word() is "三二一")
# k1.read_word()

k2 = new READING_BUFFER "k2", "(define lambda)"
asr(k2.read_word() is "(")
asr(k2.read_word() is "define")
asr(k2.read_word() is "lambda")
asr(k2.read_word() is ")")
fs = require("fs")

file_to_reading_buffer = (file_name) ->
  try
    new READING_BUFFER file_name,
      fs.readFileSync(file_name).toString()
  catch error
    orz error

# cat(file_to_reading_buffer("README"))
current_reading_buffer = null
assembler = (file_name) ->
  current_reading_buffer = file_to_reading_buffer(file_name)
  cat "- assembling:", file_name
  while not current_reading_buffer.space()
    word = current_reading_buffer.read_word()
    process_word(word)
debugging = true
assembler "test/test.cn"
module.exports = {

}
